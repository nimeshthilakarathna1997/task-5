import java.util.List;
import java.util.stream.Collectors;

public class Main {

	public static void main(String[] args) {
		List<CricketPlayer> players = CricketPlayerDataReader
				.readPlayersFromFile("C:/Users/nlt19/eclipse-workspace/Task 5/src/players.csv");

		System.out.println("---Calculating Total Runs---");
		long startTime = System.currentTimeMillis();
		int totalRunsSequential = players.stream().mapToInt(CricketPlayer::getRuns).sum();
		long endTime = System.currentTimeMillis();
		long sequentialTime = endTime - startTime;

		// Calculate total runs using parallel stream
		startTime = System.currentTimeMillis();
		int totalRunsParallel = players.parallelStream().mapToInt(CricketPlayer::getRuns).sum();
		endTime = System.currentTimeMillis();
		long parallelTime = endTime - startTime;

		// Print the total runs and execution times
		System.out.println("Total Runs (Sequential): " + totalRunsSequential);
		System.out.println("Total Runs (Parallel): " + totalRunsParallel);
		System.out.println("Sequential Time: " + sequentialTime + " ms");
		System.out.println("Parallel Time: " + parallelTime + " ms");

		System.out.println();
		System.out.println();
		
		System.out.println("---Perform sorting based on player name---");
		startTime = System.currentTimeMillis();
		List<CricketPlayer> sortedSequential = players.stream()
				.sorted((player1, player2) -> player1.getPlayerName().compareTo(player2.getPlayerName()))
				.collect(Collectors.toList());
		endTime = System.currentTimeMillis();
		sequentialTime = endTime - startTime;

		// Perform sorting based on player name using parallel stream
		startTime = System.currentTimeMillis();
		List<CricketPlayer> sortedParallel = players.parallelStream()
				.sorted((player1, player2) -> player1.getPlayerName().compareTo(player2.getPlayerName()))
				.collect(Collectors.toList());
		endTime = System.currentTimeMillis();
		parallelTime = endTime - startTime;

		// Print the execution times
		System.out.println("\nSequential Time: " + sequentialTime + " ms");
		System.out.println("Parallel Time: " + parallelTime + " ms");
		
		System.out.println();
		System.out.println();
		
		System.out.println("---Finding the player with most runs using compareTo method---");
		startTime = System.currentTimeMillis();
		CricketPlayer highestScorerSequential = players
				.stream().max(CricketPlayer::compareTo)
				.orElse(null);
		endTime = System.currentTimeMillis();
		sequentialTime = endTime - startTime;

		startTime = System.currentTimeMillis();
		CricketPlayer highestScorerParallel = players.parallelStream()
				.max(CricketPlayer::compareTo)
				.orElse(null);
		endTime = System.currentTimeMillis();
		parallelTime = endTime - startTime;

		System.out.println("Most Runs (Sequential): " + highestScorerSequential);
		System.out.println("Most Runs (Parallel): " + highestScorerParallel);
		
		System.out.println("Sequential Time: " + sequentialTime + " ms");
		System.out.println("Parallel Time: " + parallelTime + " ms");

		System.out.println();
		System.out.println();

		
		System.out.println("---Filtering players with more than 5 centuries---");
		int minCenturies = 5; // Minimum number of centuries

		startTime = System.currentTimeMillis();
		List<CricketPlayer> filteredPlayersSequential = players.stream()
				.filter(player -> player.getCenturies() > minCenturies).collect(Collectors.toList());
		endTime = System.currentTimeMillis();
		sequentialTime = endTime - startTime;

		// Filtering using parallel stream
		startTime = System.currentTimeMillis();
		List<CricketPlayer> filteredPlayersParallel = players.parallelStream()
				.filter(player -> player.getCenturies() > minCenturies).collect(Collectors.toList());
		endTime = System.currentTimeMillis();
		parallelTime = endTime - startTime;

		// Print the execution times
		System.out.println("\nSequential Time: " + sequentialTime + " ms");
		System.out.println("Parallel Time: " + parallelTime + " ms");

	}

}
