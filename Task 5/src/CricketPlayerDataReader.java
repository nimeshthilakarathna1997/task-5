import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class CricketPlayerDataReader {

    private static final String CSV_SEPARATOR = ",";

    public static List<CricketPlayer> readPlayersFromFile(String filePath) {
        List<CricketPlayer> players = new ArrayList<>();

        try (BufferedReader reader = new BufferedReader(new FileReader(filePath))) {
            players = reader.lines()
                    .skip(1) // Skip the first row
                    .map(line -> line.split(CSV_SEPARATOR))
                    .map(CricketPlayerDataReader::createPlayerFromData)
                    .collect(Collectors.toList());
        } catch (IOException e) {
            e.printStackTrace();
        }

        return players;
    }

    private static CricketPlayer createPlayerFromData(String[] data) {
        String playerName = data[1]; // Skip the first column
        String span = data[2];
        int matches = parseIntegerOrDefault(data[3], 0); // Use 0 as the default value
        int innings = parseIntegerOrDefault(data[4], 0); // Use 0 as the default value
        int playerNumber = parseIntegerOrDefault(data[5], 0); // Use 0 as the default value
        int runs = parseIntegerOrDefault(data[6], -1); // Use 0 as the default value
        String highestScore = data[7];
        double average = parseDoubleOrDefault(data[8], 0); // Use 0 as the default value
        int ballsFaced = parseIntegerOrDefault(data[9], 0); // Use 0 as the default value
        double strikeRate = parseDoubleOrDefault(data[10], 0); // Use 0 as the default value
        int centuries = parseIntegerOrDefault(data[11], 0); // Use 0 as the default value
        int fifties = parseIntegerOrDefault(data[12], 0); // Use 0 as the default value
        int zeros = parseIntegerOrDefault(data[13], 0); // Use 0 as the default value

        return new CricketPlayer(playerName, span, matches, innings, playerNumber, runs, highestScore,
                average, ballsFaced, strikeRate, centuries, fifties, zeros);
    }

    private static int parseIntegerOrDefault(String value, int defaultValue) {
        try {
            return Integer.parseInt(value);
        } catch (NumberFormatException e) {
            return defaultValue;
        }
    }

    private static double parseDoubleOrDefault(String value, double defaultValue) {
        if (value.equals("-")) {
            return defaultValue;
        }
        return Double.parseDouble(value);
    }
}
