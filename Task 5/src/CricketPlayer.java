public class CricketPlayer implements Comparable<CricketPlayer> {
    private String playerName;
    private String span;
    private int matches;
    private int innings;
    private int playerNumber;
    private int runs;
    private String highestScore;
    private double average;
    private int ballsFaced;
    private double strikeRate;
    private int centuries;
    private int fifties;
    private int zeros;

    public CricketPlayer(String playerName, String span, int matches, int innings,
                         int playerNumber, int runs, String highestScore, double average, int ballsFaced,
                         double strikeRate, int centuries, int fifties, int zeros) {
        this.playerName = playerName;
        this.span = span;
        this.matches = matches;
        this.innings = innings;
        this.playerNumber = playerNumber;
        this.runs = runs;
        this.highestScore = highestScore;
        this.average = average;
        this.ballsFaced = ballsFaced;
        this.strikeRate = strikeRate;
        this.centuries = centuries;
        this.fifties = fifties;
        this.zeros = zeros;
    }

    public String getPlayerName() {
        return playerName;
    }

    public void setPlayerName(String playerName) {
        this.playerName = playerName;
    }

    public String getSpan() {
        return span;
    }

    public void setSpan(String span) {
        this.span = span;
    }

    public int getMatches() {
        return matches;
    }

    public void setMatches(int matches) {
        this.matches = matches;
    }

    public int getInnings() {
        return innings;
    }

    public void setInnings(int innings) {
        this.innings = innings;
    }

    public int getPlayerNumber() {
        return playerNumber;
    }

    public void setPlayerNumber(int playerNumber) {
        this.playerNumber = playerNumber;
    }

    public int getRuns() {
        return runs;
    }

    public void setRuns(int runs) {
        this.runs = runs;
    }

    public String getHighestScore() {
        return highestScore;
    }

    public void setHighestScore(String highestScore) {
        this.highestScore = highestScore;
    }

    public double getAverage() {
        return average;
    }

    public void setAverage(double average) {
        this.average = average;
    }

    public int getBallsFaced() {
        return ballsFaced;
    }

    public void setBallsFaced(int ballsFaced) {
        this.ballsFaced = ballsFaced;
    }

    public double getStrikeRate() {
        return strikeRate;
    }

    public void setStrikeRate(double strikeRate) {
        this.strikeRate = strikeRate;
    }

    public int getCenturies() {
        return centuries;
    }

    public void setCenturies(int centuries) {
        this.centuries = centuries;
    }

    public int getFifties() {
        return fifties;
    }

    public void setFifties(int fifties) {
        this.fifties = fifties;
    }

    public int getZeros() {
        return zeros;
    }

    public void setZeros(int zeros) {
        this.zeros = zeros;
    }

    @Override
    public int compareTo(CricketPlayer otherPlayer) {
        return Integer.compare(runs, otherPlayer.runs);
    }
    
    @Override
    public String toString() {
        return "Player Name: " + playerName +
                "\nMatches: " + matches +
                "\nInnings: " + innings +
                "\nPlayer Number: " + playerNumber +
                "\nRuns: " + runs +
                "\nHighest Score: " + highestScore;
    }
}
